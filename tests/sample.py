import subprocess


def test_gpu():
    result = subprocess.run(["nvidia-smi", "-L"], stdout=subprocess.PIPE)
    assert "GPU" in result.stdout.decode()
